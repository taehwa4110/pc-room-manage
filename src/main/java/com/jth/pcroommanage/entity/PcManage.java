package com.jth.pcroommanage.entity;

import com.jth.pcroommanage.enums.PcPlace;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import com.jth.pcroommanage.model.pcmanage.PcManageBuyRequest;
import com.jth.pcroommanage.model.pcmanage.PcManagePartChangeRequest;
import com.jth.pcroommanage.model.pcmanage.PcManagePlaceChangeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcManage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer pcNumber;

    @Column(nullable = false, length = 30)
    private String mouseName;

    @Column(nullable = false, length = 30)
    private String keyboardName;

    @Column(nullable = false, length = 30)
    private String monitorName;

    @Column(nullable = false, length = 30)
    private String headsetName;

    @Column(nullable = false, length = 30)
    private String speakerName;

    @Column(nullable = false, length = 30)
    private String cpuName;

    @Column(nullable = false, length = 30)
    private String ramName;

    @Column(nullable = false, length = 30)
    private String graphicsCardName;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PcPlace pcPlace;

    @Column(nullable = false)
    private Integer pcPerMinutePrice;

    @Column(nullable = false)
    private Boolean isUsed;

    @Column(nullable = false)
    private LocalDateTime dateStart;

    private LocalDateTime dateBroken;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putPcBroken() {
        this.isUsed = false;
        this.dateBroken = LocalDateTime.now();
    }

    public void putPcPart(PcManagePartChangeRequest request) {
        this.mouseName = request.getMouseName();
        this.keyboardName = request.getKeyboardName();
        this.monitorName = request.getMonitorName();
        this.headsetName = request.getHeadsetName();
        this.speakerName = request.getSpeakerName();
        this.cpuName = request.getCpuName();
        this.ramName = request.getRamName();
        this.graphicsCardName = request.getGraphicsCardName();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putPcPlace(PcManagePlaceChangeRequest request) {
        this.pcNumber = request.getPcNumber();
        this.pcPlace = request.getPcPlace();
        this.pcPerMinutePrice = request.getPcPerMinutePrice();
        this.dateUpdate = LocalDateTime.now();
    }


    private PcManage(PcManageBuilder builder) {
        this.pcNumber = builder.pcNumber;
        this.mouseName = builder.mouseName;
        this.keyboardName = builder.keyboardName;
        this.monitorName = builder.monitorName;
        this.headsetName = builder.headsetName;
        this.speakerName = builder.speakerName;
        this.cpuName = builder.cpuName;
        this.ramName = builder.ramName;
        this.graphicsCardName = builder.graphicsCardName;
        this.pcPlace = builder.pcPlace;
        this.pcPerMinutePrice = builder.pcPerMinutePrice;
        this.isUsed = builder.isUsed;
        this.dateStart = builder.dateStart;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class PcManageBuilder implements CommonModelBuilder<PcManage> {
        private final Integer pcNumber;
        private final String mouseName;
        private final String keyboardName;
        private final String monitorName;
        private final String headsetName;
        private final String speakerName;
        private final String cpuName;
        private final String ramName;
        private final String graphicsCardName;
        private final PcPlace pcPlace;
        private final Integer pcPerMinutePrice;
        private final Boolean isUsed;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public PcManageBuilder(PcManageBuyRequest request) {
            this.pcNumber = request.getPcNumber();
            this.mouseName = request.getMouseName();
            this.keyboardName = request.getKeyboardName();
            this.monitorName = request.getMonitorName();
            this.headsetName = request.getHeadsetName();
            this.speakerName = request.getSpeakerName();
            this.cpuName = request.getCpuName();
            this.ramName = request.getRamName();
            this.graphicsCardName = request.getGraphicsCardName();
            this.pcPlace = request.getPcPlace();
            this.pcPerMinutePrice = request.getPcPerMinutePrice();
            this.isUsed = true;
            this.dateStart = LocalDateTime.now();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public PcManage build() {
            return new PcManage(this);
        }
    }
}
