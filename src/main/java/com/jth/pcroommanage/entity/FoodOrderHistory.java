package com.jth.pcroommanage.entity;

import com.jth.pcroommanage.enums.CurrentSituation;
import com.jth.pcroommanage.enums.PaymentMethod;
import com.jth.pcroommanage.enums.PaymentType;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderCurrentSituationChangeRequest;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodOrderHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcManageId", nullable = false)
    private PcManage pcManage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "foodMenuId", nullable = false)
    private FoodMenu foodMenu;

    @Column(nullable = false)
    private Integer orderCount;

    @Column(nullable = false)
    private Integer foodTotalAmount;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private PaymentType paymentType;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PaymentMethod paymentMethod;

    @Column(nullable = false, length = 20)
    private CurrentSituation foodCurrentSituation;

    @Column(nullable = false, length = 50)
    private String customerRequests;

    @Column(nullable = false)
    private LocalDateTime dateOrder;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putFoodCurrentSituation(FoodOrderCurrentSituationChangeRequest request) {
        this.foodCurrentSituation = request.getFoodCurrentSituation();
        this.dateUpdate = LocalDateTime.now();
    }

    private FoodOrderHistory(FoodOrderHistoryBuilder builder) {
        this.pcManage = builder.pcManage;
        this.foodMenu = builder.foodMenu;
        this.orderCount = builder.orderCount;
        this.foodTotalAmount = builder.foodTotalAmount;
        this.paymentType = builder.paymentType;
        this.paymentMethod = builder.paymentMethod;
        this.foodCurrentSituation = builder.foodCurrentSituation;
        this.customerRequests = builder.customerRequests;
        this.dateOrder = builder.dateOrder;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class FoodOrderHistoryBuilder implements CommonModelBuilder<FoodOrderHistory> {
        private final PcManage pcManage;
        private final FoodMenu foodMenu;
        private final Integer orderCount;
        private final Integer foodTotalAmount;
        private final PaymentType paymentType;
        private final PaymentMethod paymentMethod;
        private final CurrentSituation foodCurrentSituation;
        private final String customerRequests;
        private final LocalDateTime dateOrder;
        private final LocalDateTime dateUpdate;

        public FoodOrderHistoryBuilder(PcManage pcManage, FoodMenu foodMenu, FoodOrderHistoryRequest request) {
            this.pcManage = pcManage;
            this.foodMenu = foodMenu;
            this.orderCount = request.getFoodTotalAmount();
            this.foodTotalAmount = request.getFoodTotalAmount();
            this.paymentType = request.getPaymentType();
            this.paymentMethod = request.getPaymentMethod();
            this.foodCurrentSituation = request.getFoodCurrentSituation();
            this.customerRequests = request.getCustomerRequests();
            this.dateOrder = request.getDateOrder();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public FoodOrderHistory build() {
            return new FoodOrderHistory(this);
        }
    }
}
