package com.jth.pcroommanage.entity;

import com.jth.pcroommanage.enums.PcPartType;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import com.jth.pcroommanage.model.pcpartchangehistory.PcPartChangeHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcPartChangeHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcManageId", nullable = false)
    private PcManage pcManage;

    @Column(nullable = false, length = 20)
    private PcPartType pcPartType;

    @Column(nullable = false, length = 20)
    private String pcPartName;

    @Column(nullable = false)
    private Double pcPartPrice;

    @Column(nullable = false)
    private LocalDateTime dateChange;

    private PcPartChangeHistory(PcPartChangeHistoryBuilder builder) {
        this.pcManage = builder.pcManage;
        this.pcPartType = builder.pcPartType;
        this.pcPartName = builder.pcPartName;
        this.pcPartPrice = builder.pcPartPrice;
        this.dateChange = builder.dateChange;
    }
    public static class PcPartChangeHistoryBuilder implements CommonModelBuilder<PcPartChangeHistory> {
        private final PcManage pcManage;
        private final PcPartType pcPartType;
        private final String pcPartName;
        private final Double pcPartPrice;
        private final LocalDateTime dateChange;

        public PcPartChangeHistoryBuilder(PcManage pcManage, PcPartChangeHistoryRequest request) {
            this.pcManage = pcManage;
            this.pcPartType = request.getPcPartType();
            this.pcPartName = request.getPcPartName();
            this.pcPartPrice = request.getPcPartPrice();
            this.dateChange = request.getDateChange();
        }

        @Override
        public PcPartChangeHistory build() {
            return new PcPartChangeHistory(this);
        }
    }
}
