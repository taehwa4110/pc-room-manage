package com.jth.pcroommanage.entity;

import com.jth.pcroommanage.enums.PaymentMethod;
import com.jth.pcroommanage.enums.PcPower;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import com.jth.pcroommanage.model.pcusagehistory.PcUsageHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcUsageHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcManageId", nullable = false)
    private PcManage pcManage;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PcPower pcPower;

    @Column(nullable = false)
    private LocalDateTime pcUsageStart;

    @Column(nullable = false)
    private LocalDateTime pcUsageEnd;

    @Column(nullable = false)
    private Long pcUsageTotalAmount;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PaymentMethod paymentMethod;

    private PcUsageHistory(PcUsageHistoryBuilder builder) {
        this.pcManage = builder.pcManage;
        this.pcPower = builder.pcPower;
        this.pcUsageStart = builder.pcUsageStart;
        this.pcUsageEnd = builder.pcUsageEnd;
        this.pcUsageTotalAmount = builder.pcUsageTotalAmount;
        this.paymentMethod = builder.paymentMethod;
    }
    public static class PcUsageHistoryBuilder implements CommonModelBuilder<PcUsageHistory> {
        private final PcManage pcManage;
        private final PcPower pcPower;
        private final LocalDateTime pcUsageStart;
        private final LocalDateTime pcUsageEnd;
        private final Long pcUsageTotalAmount;
        private final PaymentMethod paymentMethod;

        public PcUsageHistoryBuilder(PcManage pcManage, PcUsageHistoryRequest request) {
            this.pcManage = pcManage;
            this.pcPower = request.getPcPower();
            this.pcUsageStart = request.getPcUsageStart();
            this.pcUsageEnd = request.getPcUsageEnd();
            Duration duration = Duration.between(pcUsageStart, pcUsageEnd);
            this.pcUsageTotalAmount = pcManage.getPcPerMinutePrice() * duration.toMinutes();
            this.paymentMethod = request.getPaymentMethod();
        }

        @Override
        public PcUsageHistory build() {
            return new PcUsageHistory(this);
        }
    }
}
