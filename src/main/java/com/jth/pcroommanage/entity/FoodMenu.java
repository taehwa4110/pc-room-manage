package com.jth.pcroommanage.entity;

import com.jth.pcroommanage.enums.FoodType;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import com.jth.pcroommanage.model.foodmenu.FoodMenuRequest;
import com.jth.pcroommanage.model.foodmenu.FoodPriceUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100)
    private String foodImage;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private FoodType foodType;

    @Column(nullable = false, length = 20)
    private String foodName;

    @Column(nullable = false)
    private Integer foodPrice;

    @Column(nullable = false)
    private Boolean isSale;

    @Column(nullable = false)
    private LocalDateTime dateSaleStart;

    private LocalDateTime dateSaleClose;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putFoodPrice(FoodPriceUpdateRequest request) {
        this.foodPrice = request.getFoodPrice();
        this.dateUpdate = LocalDateTime.now();
    }

    public void putSaleClose() {
        this.isSale = false;
        this.dateSaleClose = LocalDateTime.now();
    }

    private FoodMenu(FoodMenuBuilder builder) {
        this.foodImage = builder.foodImage;
        this.foodType = builder.foodType;
        this.foodName = builder.foodName;
        this.foodPrice = builder.foodPrice;
        this.isSale = builder.isSale;
        this.dateSaleStart = builder.dateSaleStart;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class FoodMenuBuilder implements CommonModelBuilder<FoodMenu> {
        private final String foodImage;
        private final FoodType foodType;
        private final String foodName;
        private final Integer foodPrice;
        private final Boolean isSale;
        private final LocalDateTime dateSaleStart;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public FoodMenuBuilder(FoodMenuRequest request) {
            this.foodImage = request.getFoodImage();
            this.foodType = request.getFoodType();
            this.foodName = request.getFoodName();
            this.foodPrice = request.getFoodPrice();
            this.isSale = true;
            this.dateSaleStart = LocalDateTime.now();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public FoodMenu build() {
            return new FoodMenu(this);
        }
    }
}
