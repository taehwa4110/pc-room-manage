package com.jth.pcroommanage.reository;

import com.jth.pcroommanage.entity.PcUsageHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface PcUsageHistoryRepository extends JpaRepository<PcUsageHistory, Long> {
    List<PcUsageHistory> findAllByPcUsageStartGreaterThanEqualAndPcUsageEndLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
