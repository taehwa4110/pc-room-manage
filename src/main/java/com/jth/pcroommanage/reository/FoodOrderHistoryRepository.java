package com.jth.pcroommanage.reository;

import com.jth.pcroommanage.entity.FoodOrderHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface FoodOrderHistoryRepository extends JpaRepository<FoodOrderHistory, Long> {
    List<FoodOrderHistory> findAllByDateOrderGreaterThanEqualAndDateOrderLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
