package com.jth.pcroommanage.reository;

import com.jth.pcroommanage.entity.FoodMenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodMenuRepository extends JpaRepository<FoodMenu, Long> {
}
