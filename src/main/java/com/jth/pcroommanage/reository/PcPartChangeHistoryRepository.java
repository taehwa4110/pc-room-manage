package com.jth.pcroommanage.reository;

import com.jth.pcroommanage.entity.PcPartChangeHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PcPartChangeHistoryRepository extends JpaRepository<PcPartChangeHistory, Long> {
    List<PcPartChangeHistory> findAllByDateChangeGreaterThanEqualAndDateChangeLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
