package com.jth.pcroommanage.reository;

import com.jth.pcroommanage.entity.PcManage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManageRepository extends JpaRepository<PcManage, Long> {
}
