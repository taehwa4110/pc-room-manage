package com.jth.pcroommanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcRoomManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcRoomManageApplication.class, args);
    }

}
