package com.jth.pcroommanage.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
