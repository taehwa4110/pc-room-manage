package com.jth.pcroommanage.service;

import com.jth.pcroommanage.entity.FoodMenu;
import com.jth.pcroommanage.exception.CMissingDataException;
import com.jth.pcroommanage.exception.CNoFoodDataException;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.foodmenu.FoodMenuItem;
import com.jth.pcroommanage.model.foodmenu.FoodMenuRequest;
import com.jth.pcroommanage.model.foodmenu.FoodPriceUpdateRequest;
import com.jth.pcroommanage.reository.FoodMenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodMenuService {
    private final FoodMenuRepository foodMenuRepository;

    /**
     * 음식 등록
     * @param request 음식을 등록하기 위해 필요로 하는 값
     */
    public void setFoodMenu(FoodMenuRequest request) {
        FoodMenu foodMenu = new FoodMenu.FoodMenuBuilder(request).build();
        foodMenuRepository.save(foodMenu);
    }

    /**
     * 음식 정보 주기
     * @param id 음식 시퀀스
     * @return 음식 정보
     */
    public FoodMenu getFoodMenuData(long id) {
        return foodMenuRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 음식 리스트 가져오기
     * @return 음식 리스트
     */
    public ListResult<FoodMenuItem> getFoods() {
        List<FoodMenuItem> result = new LinkedList<>();

        List<FoodMenu> foodMenus = foodMenuRepository.findAll();

        foodMenus.forEach(foodMenu -> {
            FoodMenuItem foodMenuItem = new FoodMenuItem.FoodMenuItemBuilder(foodMenu).build();
            result.add(foodMenuItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 음식 가격 수정하기
     * @param id 음식 시퀀스
     * @param request 음식 가격을 수정하기 위해 필요로 하는 값
     */
    public void putFoodPrice(long id, FoodPriceUpdateRequest request) {
        FoodMenu foodMenu = foodMenuRepository.findById(id).orElseThrow(CMissingDataException::new);
        foodMenu.putFoodPrice(request);

        foodMenuRepository.save(foodMenu);
    }

    /**
     * 음식 삭제하기
     * @param id 음식 시퀀스
     */
    public void putFoodSaleClose(long id) {
        FoodMenu foodMenu = foodMenuRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!foodMenu.getIsSale())throw new CNoFoodDataException();

        foodMenu.putSaleClose();
        foodMenuRepository.save(foodMenu);
    }
}
