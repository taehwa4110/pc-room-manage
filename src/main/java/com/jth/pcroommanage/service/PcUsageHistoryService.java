package com.jth.pcroommanage.service;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.entity.PcUsageHistory;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.pcusagehistory.PcUsageHistoryItem;
import com.jth.pcroommanage.model.pcusagehistory.PcUsageHistoryRequest;
import com.jth.pcroommanage.reository.PcUsageHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcUsageHistoryService {
    private final PcUsageHistoryRepository pcUsageHistoryRepository;

    /**
     * PC 사용내역 등록하기
     * @param pcManage PC 정보
     * @param request PC 사용내역을 등록하기 위해 필요로 하는 값
     */
    public void setPcUsageHistory(PcManage pcManage, PcUsageHistoryRequest request) {
        PcUsageHistory pcUsageHistory = new PcUsageHistory.PcUsageHistoryBuilder(pcManage, request).build();
        pcUsageHistoryRepository.save(pcUsageHistory);
    }

    /**
     * 기간에 해당되는 PC 사용내역 리스트 가져오기
     * @param dateStart 시작일
     * @param dateEnd 종료일
     * @return PC 사용내역 리스트
     */
    public ListResult<PcUsageHistoryItem> getPcUsageHistory(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<PcUsageHistory> pcUsageHistories = pcUsageHistoryRepository.findAllByPcUsageStartGreaterThanEqualAndPcUsageEndLessThanEqualOrderByIdDesc(dateStartTime,dateEndTime);

        List<PcUsageHistoryItem> result = new LinkedList<>();

        pcUsageHistories.forEach(pcUsageHistory -> {
            PcUsageHistoryItem pcUsageHistoryItem = new PcUsageHistoryItem.PcUsageHistoryItemBuilder(pcUsageHistory).build();
            result.add(pcUsageHistoryItem);
        });

        return ListConvertService.settingResult(result);
    }
}
