package com.jth.pcroommanage.service;

import com.jth.pcroommanage.entity.FoodMenu;
import com.jth.pcroommanage.entity.FoodOrderHistory;
import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.exception.CMissingDataException;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderCurrentSituationChangeRequest;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderHistoryItem;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderHistoryRequest;
import com.jth.pcroommanage.reository.FoodOrderHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodOrderHistoryService {
    private final FoodOrderHistoryRepository foodOrderHistoryRepository;

    /**
     * 음식 주문내역 등록하기
     * @param pcManage PC 정보
     * @param foodMenu 음식 정보
     * @param request 음식 주문내역을 등록하기 위해 필요로 하는 값
     */
    public void setFoodOrderHistory(PcManage pcManage, FoodMenu foodMenu, FoodOrderHistoryRequest request) {
        FoodOrderHistory foodOrderHistory = new FoodOrderHistory.FoodOrderHistoryBuilder(pcManage, foodMenu, request).build();
        foodOrderHistoryRepository.save(foodOrderHistory);
    }

    /**
     * 기간에 해당되는 음식 주문 내역 리스트 가져오기
     * @param dateStart 시작일
     * @param dateEnd 종료일
     * @return 음식 주문 내역 리스트
     */
    public ListResult<FoodOrderHistoryItem> getFoodOrderHistory(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<FoodOrderHistory> foodOrderHistories = foodOrderHistoryRepository.findAllByDateOrderGreaterThanEqualAndDateOrderLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<FoodOrderHistoryItem> result = new LinkedList<>();

        foodOrderHistories.forEach(foodOrderHistory -> {
            FoodOrderHistoryItem foodOrderHistoryItem = new FoodOrderHistoryItem.FoodOrderHistoryItemBuilder(foodOrderHistory).build();
            result.add(foodOrderHistoryItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 음식 현재 상황 수정하기
     * @param id 음식 주문내역 시퀀스
     * @param request 음식 현재 상황을 수정하기 위해 필요로 하는 값
     */
    public void putFoodOrderCurrentSituation(long id, FoodOrderCurrentSituationChangeRequest request) {
        FoodOrderHistory foodOrderHistory = foodOrderHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
        foodOrderHistory.putFoodCurrentSituation(request);

        foodOrderHistoryRepository.save(foodOrderHistory);
    }
}
