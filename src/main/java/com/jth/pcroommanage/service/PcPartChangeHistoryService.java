package com.jth.pcroommanage.service;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.entity.PcPartChangeHistory;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.pcpartchangehistory.PcPartChangeHistoryItem;
import com.jth.pcroommanage.model.pcpartchangehistory.PcPartChangeHistoryRequest;
import com.jth.pcroommanage.reository.PcPartChangeHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcPartChangeHistoryService {
    private final PcPartChangeHistoryRepository pcPartChangeHistoryRepository;

    /**
     * PC 부품 교체내역 등록
     * @param pcManage PC 정보
     * @param request PC 부품 교체내역을 등록하기 위해 필요로 하는 값
     */
    public void setPcPartChangeHistory(PcManage pcManage, PcPartChangeHistoryRequest request) {
        PcPartChangeHistory pcPartChangeHistory = new PcPartChangeHistory.PcPartChangeHistoryBuilder(pcManage, request).build();
        pcPartChangeHistoryRepository.save(pcPartChangeHistory);
    }

    /**
     * 기간에 해당되는 PC 부품 교체내역 리스트 가져오기
     * @param dateStart 시작일
     * @param dateEnd 종료일
     * @return PC 부품 교체내역 리스트
     */
    public ListResult<PcPartChangeHistoryItem> getPcPartChangeHistories(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<PcPartChangeHistory> pcPartChangeHistories = pcPartChangeHistoryRepository.findAllByDateChangeGreaterThanEqualAndDateChangeLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<PcPartChangeHistoryItem> result = new LinkedList<>();

        pcPartChangeHistories.forEach(pcPartChangeHistory -> {
            PcPartChangeHistoryItem pcPartChangeHistoryItem = new PcPartChangeHistoryItem.PcPartChangeHistoryItemBuilder(pcPartChangeHistory).build();
            result.add(pcPartChangeHistoryItem);
        });

        return ListConvertService.settingResult(result);

    }
}
