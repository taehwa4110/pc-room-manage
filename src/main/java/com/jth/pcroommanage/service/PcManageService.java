package com.jth.pcroommanage.service;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.exception.CMissingDataException;
import com.jth.pcroommanage.exception.CNoPcDataException;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.pcmanage.*;
import com.jth.pcroommanage.reository.PcManageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManageService {
    private final PcManageRepository pcManageRepository;

    /**
     * PC 등록
     * @param request PC 등록을 위해 필요로 하는 값
     */
    public void setPc(PcManageBuyRequest request) {
        PcManage pcManage = new PcManage.PcManageBuilder(request).build();
        pcManageRepository.save(pcManage);
    }

    /**
     * PC 정보 주기
     * @param id PC 시퀀스
     * @return PC 정보
     */
    public PcManage getPcData(long id) {
        return pcManageRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * PC 상세 정보 가져오기
     * @param id PC 시퀀스
     * @return PC 상세 정보
     */
    public PcManageDetail getPc(long id) {
        PcManage pcManage = pcManageRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new PcManageDetail.PcDetailBuilder(pcManage).build();
    }

    /**
     * PC 리스트 가져오기
     * @return PC 리스트
     */
    public ListResult<PcManageItem> getPcs() {
        List<PcManageItem> result = new LinkedList<>();

        List<PcManage> pcManages = pcManageRepository.findAll();

        pcManages.forEach(pcManage -> {
            PcManageItem pcItem = new PcManageItem.PcItemBuilder(pcManage).build();
            result.add(pcItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * PC 부품 및 사양 교체하기
     * @param id PC 시퀀스
     * @param request PC 부품 및 사양을 교체하기 위해 필요로 하는 값
     */
    public void putPcPart(long id, PcManagePartChangeRequest request) {
        PcManage pcManage = pcManageRepository.findById(id).orElseThrow(CMissingDataException::new);
        pcManage.putPcPart(request);

        pcManageRepository.save(pcManage);
    }

    /**
     * PC 설치 위치 수정하기
     * @param id PC 시퀀스
     * @param request PC 설치 위치 수정하기 위해 필요로 하는 값
     */
    public void putPcPlace(long id, PcManagePlaceChangeRequest request) {
        PcManage pcManage = pcManageRepository.findById(id).orElseThrow(CMissingDataException::new);
        pcManage.putPcPlace(request);

        pcManageRepository.save(pcManage);
    }

    /**
     * PC 정보 삭제하기
     * @param id PC 시퀀스
     */
    public void putPcBroken(long id) {
        PcManage pcManage = pcManageRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!pcManage.getIsUsed())throw new CNoPcDataException();

        pcManage.putPcBroken();
        pcManageRepository.save(pcManage);
    }


}
