package com.jth.pcroommanage.exception;

public class CNoFoodDataException extends RuntimeException{
    public CNoFoodDataException(String msg, Throwable t) {super(msg, t);}

    public CNoFoodDataException(String msg) {
        super(msg);
    }

    public CNoFoodDataException() {
        super();
    }
}
