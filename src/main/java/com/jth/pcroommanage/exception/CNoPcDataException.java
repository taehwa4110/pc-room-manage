package com.jth.pcroommanage.exception;

public class CNoPcDataException extends RuntimeException{
    public CNoPcDataException(String msg, Throwable t) {super(msg, t);}

    public CNoPcDataException(String msg) {
        super(msg);
    }

    public CNoPcDataException() {
        super();
    }
}
