package com.jth.pcroommanage.model.foodorderhistory;

import com.jth.pcroommanage.enums.CurrentSituation;
import com.jth.pcroommanage.enums.PaymentMethod;
import com.jth.pcroommanage.enums.PaymentType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class FoodOrderHistoryRequest {
    @ApiModelProperty(notes = "PC 시퀀스", required = true)
    @NotNull
    private Long pcManageId;

    @ApiModelProperty(notes = "음식 메뉴 시퀀스", required = true)
    @NotNull
    private Long foodMenuId;

    @ApiModelProperty(notes = "음식 주문 개수", required = true)
    @NotNull
    private Integer orderCount;

    @ApiModelProperty(notes = "음식 총 금액", required = true)
    @NotNull
    private Integer foodTotalAmount;

    @ApiModelProperty(notes = "지불 종류", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PaymentType paymentType;

    @ApiModelProperty(notes = "지불 방법", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PaymentMethod paymentMethod;

    @ApiModelProperty(notes = "음식 현재 상황 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private CurrentSituation foodCurrentSituation;

    @ApiModelProperty(notes = "고객 요청사항", required = true)
    @NotNull
    @Length(max = 50)
    private String customerRequests;

    @ApiModelProperty(notes = "주문 날짜", required = true)
    @NotNull
    private LocalDateTime dateOrder;
}
