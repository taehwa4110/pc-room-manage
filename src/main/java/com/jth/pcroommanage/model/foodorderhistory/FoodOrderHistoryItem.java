package com.jth.pcroommanage.model.foodorderhistory;

import com.jth.pcroommanage.entity.FoodOrderHistory;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodOrderHistoryItem {
    @ApiModelProperty(notes = "음식 주문 내역 시퀀스")
    private Long foodOrderHistoryId;
    @ApiModelProperty(notes = "PC 위치 및 번호")
    private String pcFullName;
    @ApiModelProperty(notes = "음식 종류와 음식명")
    private String foodFullName;
    @ApiModelProperty(notes = "음식 가격")
    private String foodPrice;
    @ApiModelProperty(notes = "음식 주문 갯수")
    private Integer orderCount;
    @ApiModelProperty(notes = "음식 총 금액")
    private String foodTotalAmount;
    @ApiModelProperty(notes = "지불 종류")
    private String paymentType;
    @ApiModelProperty(notes = "지불 방법")
    private String paymentMethod;
    @ApiModelProperty(notes = "음식 현재 상황")
    private String foodCurrentSituation;
    @ApiModelProperty(notes = "고객 요청사항")
    private String customerRequests;
    @ApiModelProperty(notes = "주문 날짜")
    private LocalDateTime dateOrder;

    private FoodOrderHistoryItem(FoodOrderHistoryItemBuilder builder) {
        this.foodOrderHistoryId = builder.foodOrderHistoryId;
        this.pcFullName = builder.pcFullName;
        this.foodFullName = builder.foodFullName;
        this.foodPrice = builder.foodPrice;
        this.orderCount = builder.orderCount;
        this.foodTotalAmount = builder.foodTotalAmount;
        this.paymentType = builder.paymentType;
        this.paymentMethod = builder.paymentMethod;
        this.foodCurrentSituation = builder.foodCurrentSituation;
        this.customerRequests = builder.customerRequests;
        this.dateOrder = builder.dateOrder;
    }

    public static class FoodOrderHistoryItemBuilder implements CommonModelBuilder<FoodOrderHistoryItem> {
        private final Long foodOrderHistoryId;
        private final String pcFullName;
        private final String foodFullName;
        private final String foodPrice;
        private final Integer orderCount;
        private final String foodTotalAmount;
        private final String paymentType;
        private final String paymentMethod;
        private final String foodCurrentSituation;
        private final String customerRequests;
        private final LocalDateTime dateOrder;

        public FoodOrderHistoryItemBuilder(FoodOrderHistory foodOrderHistory) {
            this.foodOrderHistoryId = foodOrderHistory.getId();
            this.pcFullName = "[" + foodOrderHistory.getPcManage().getPcPlace().getName() + "]" + foodOrderHistory.getPcManage().getPcNumber();
            this.foodFullName = "[" + foodOrderHistory.getFoodMenu().getFoodType().getName() + "]" + foodOrderHistory.getFoodMenu().getFoodName();
            this.foodPrice = foodOrderHistory.getFoodMenu().getFoodPrice() + "원";
            this.orderCount = foodOrderHistory.getOrderCount();
            this.foodTotalAmount = foodOrderHistory.getFoodMenu().getFoodPrice() * foodOrderHistory.getFoodTotalAmount() + "원";
            this.paymentType = foodOrderHistory.getPaymentType().getName();
            this.paymentMethod = foodOrderHistory.getPaymentMethod().getName();
            this.foodCurrentSituation = foodOrderHistory.getFoodCurrentSituation().getName();
            this.customerRequests = foodOrderHistory.getCustomerRequests();
            this.dateOrder = foodOrderHistory.getDateOrder();
        }

        @Override
        public FoodOrderHistoryItem build() {
            return new FoodOrderHistoryItem(this);
        }
    }
}
