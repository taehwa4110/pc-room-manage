package com.jth.pcroommanage.model.foodorderhistory;

import com.jth.pcroommanage.enums.CurrentSituation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FoodOrderCurrentSituationChangeRequest {
    @ApiModelProperty(notes = "음식 현재 상황 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private CurrentSituation foodCurrentSituation;
}
