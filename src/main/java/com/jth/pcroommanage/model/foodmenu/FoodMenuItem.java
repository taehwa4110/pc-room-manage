package com.jth.pcroommanage.model.foodmenu;

import com.jth.pcroommanage.entity.FoodMenu;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodMenuItem {
    @ApiModelProperty(notes = "음식 메뉴 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "음식 분류 및 음식명")
    private String foodFullName;
    @ApiModelProperty(notes = "음식 이미지")
    private String foodImage;
    @ApiModelProperty(notes = "음식 가격")
    private String foodPrice;

    @ApiModelProperty(notes = "음식 판매 여부")
    private String isSale;

    @ApiModelProperty(notes = "음식 판매 시작일")
    private LocalDateTime dateSaleStart;

    @ApiModelProperty(notes = "음식 판매 중단일")
    private LocalDateTime dateSaleClose;

    private FoodMenuItem(FoodMenuItemBuilder builder) {
        this.id = builder.id;
        this.foodFullName = builder.foodFullName;
        this.foodImage = builder.foodImage;
        this.foodPrice = builder.foodPrice;
        this.isSale = builder.isSale;
        this.dateSaleStart = builder.dateSaleStart;
        this.dateSaleClose = builder.dateSaleClose;
    }

    public static class FoodMenuItemBuilder implements CommonModelBuilder<FoodMenuItem> {
        private final Long id;
        private final String foodFullName;
        private final String foodImage;
        private final String foodPrice;
        private final String isSale;
        private final LocalDateTime dateSaleStart;
        private final LocalDateTime dateSaleClose;

        public FoodMenuItemBuilder(FoodMenu foodMenu) {
            this.id = foodMenu.getId();
            this.foodFullName = "[" + foodMenu.getFoodType().getName() + "]" + foodMenu.getFoodName();
            this.foodImage = foodMenu.getFoodImage();
            this.foodPrice = foodMenu.getFoodPrice() + "원";
            this.isSale = foodMenu.getIsSale() ? "판매중" : "해당 음식을 판매하지않습니다.";
            this.dateSaleStart = foodMenu.getDateSaleStart();
            this.dateSaleClose = foodMenu.getDateSaleClose();
        }

        @Override
        public FoodMenuItem build() {
            return new FoodMenuItem(this);
        }
    }
}
