package com.jth.pcroommanage.model.foodmenu;

import com.jth.pcroommanage.enums.FoodType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FoodMenuRequest {
    @ApiModelProperty(notes = "음식 이미지 (~100자)", required = true)
    @NotNull
    @Length(max = 100)
    private String foodImage;

    @ApiModelProperty(notes = "음식 분류", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private FoodType foodType;

    @ApiModelProperty(notes = "음식명 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String foodName;

    @ApiModelProperty(notes = "음식 가격", required = true)
    @NotNull
    private Integer foodPrice;
}
