package com.jth.pcroommanage.model.foodmenu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FoodPriceUpdateRequest {
    @ApiModelProperty(notes = "음식 가격", required = true)
    @NotNull
    private Integer foodPrice;
}
