package com.jth.pcroommanage.model.pcusagehistory;

import com.jth.pcroommanage.enums.PaymentMethod;
import com.jth.pcroommanage.enums.PcPower;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class PcUsageHistoryRequest {

    @ApiModelProperty(notes = "PC 전원" ,required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PcPower pcPower;

    @ApiModelProperty(notes = "PC 사용 시작 시간", required = true)
    @NotNull
    private LocalDateTime pcUsageStart;

    @ApiModelProperty(notes = "PC 사용 종료 시간", required = true)
    @NotNull
    private LocalDateTime pcUsageEnd;

    @ApiModelProperty(notes = "결제 방법", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PaymentMethod paymentMethod;
}
