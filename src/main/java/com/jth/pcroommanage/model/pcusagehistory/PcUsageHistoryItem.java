package com.jth.pcroommanage.model.pcusagehistory;

import com.jth.pcroommanage.entity.PcUsageHistory;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcUsageHistoryItem {
    @ApiModelProperty(notes = "PC 사용내역 시퀀스")
    private Long pcUsageHistoryId;
    @ApiModelProperty(notes = "PC 위치 및 번호")
    private String pcFullName;
    @ApiModelProperty(notes = "PC 전원")
    private String pcPower;
    @ApiModelProperty(notes = "PC 사용 시작 시간")
    private LocalDateTime pcUsageStart;
    @ApiModelProperty(notes = "PC 사용 종료 시간")
    private LocalDateTime pcUsageEnd;
    @ApiModelProperty(notes = "PC 사용 총 금액")
    private String pcUsageTotalAmount;
    @ApiModelProperty(notes = "결재 방법")
    private String paymentMethod;


    private PcUsageHistoryItem(PcUsageHistoryItemBuilder builder) {
        this.pcUsageHistoryId = builder.pcUsageHistoryId;
        this.pcFullName = builder.pcFullName;
        this.pcPower = builder.pcPower;
        this.pcUsageStart = builder.pcUsageStart;
        this.pcUsageEnd = builder.pcUsageEnd;
        this.pcUsageTotalAmount = builder.pcUsageTotalAmount;
        this.paymentMethod = builder.paymentMethod;
    }

    public static class PcUsageHistoryItemBuilder implements CommonModelBuilder<PcUsageHistoryItem> {
        private final Long pcUsageHistoryId;
        private final String pcFullName;
        private final String pcPower;
        private final LocalDateTime pcUsageStart;
        private final LocalDateTime pcUsageEnd;
        private final String pcUsageTotalAmount;
        private final String paymentMethod;

        public PcUsageHistoryItemBuilder(PcUsageHistory pcUsageHistory) {
            this.pcUsageHistoryId = pcUsageHistory.getId();
            this.pcFullName = "[" + pcUsageHistory.getPcManage().getPcPlace().getName() + "]" + pcUsageHistory.getPcManage().getPcNumber();
            this.pcPower = pcUsageHistory.getPcPower().getName();
            this.pcUsageStart = pcUsageHistory.getPcUsageStart();
            this.pcUsageEnd = pcUsageHistory.getPcUsageEnd();
            Duration duration = Duration.between(pcUsageStart, pcUsageEnd);
            this.pcUsageTotalAmount = pcUsageHistory.getPcUsageTotalAmount() * duration.toMinutes() + "원";
            this.paymentMethod = pcUsageHistory.getPaymentMethod().getName();
        }

        @Override
        public PcUsageHistoryItem build() {
            return new PcUsageHistoryItem(this);
        }
    }
}
