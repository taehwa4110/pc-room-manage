package com.jth.pcroommanage.model.pcpartchangehistory;

import com.jth.pcroommanage.enums.PcPartType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Getter
@Setter
public class PcPartChangeHistoryRequest {
    @ApiModelProperty(notes = "PC 시퀀스", required = true)
    @NotNull
    private Long pcManageId;

    @ApiModelProperty(notes = "PC 부품 종류 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private PcPartType pcPartType;

    @ApiModelProperty(notes = "PC 부품명 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String pcPartName;

    @ApiModelProperty(notes = "PC 부품 가격", required = true)
    @NotNull
    private Double pcPartPrice;

    @ApiModelProperty(notes = "PC 부품 교체 날짜", required = true)
    @NotNull
    private LocalDateTime dateChange;
}
