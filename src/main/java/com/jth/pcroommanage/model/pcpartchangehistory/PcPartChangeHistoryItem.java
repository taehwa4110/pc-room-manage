package com.jth.pcroommanage.model.pcpartchangehistory;

import com.jth.pcroommanage.entity.PcPartChangeHistory;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcPartChangeHistoryItem {
    @ApiModelProperty(notes = "PC 부품 교체 내역 시퀀스")
    private Long pcPartChangeHistoryId;
    @ApiModelProperty(notes = "PC 위치 및 번호")
    private String pcFullName;
    @ApiModelProperty(notes = "PC 부품 종류")
    private String pcPartType;
    @ApiModelProperty(notes = "PC 부품명")
    private String pcPartName;
    @ApiModelProperty(notes = "PC 부품 가격")
    private String pcPartPrice;
    @ApiModelProperty(notes = "부품 교체 날짜")
    private LocalDateTime dateChange;

    private PcPartChangeHistoryItem(PcPartChangeHistoryItemBuilder builder) {
        this.pcPartChangeHistoryId = builder.pcPartChangeHistoryId;
        this.pcFullName = builder.pcFullName;
        this.pcPartType = builder.pcPartType;
        this.pcPartName = builder.pcPartName;
        this.pcPartPrice = builder.pcPartPrice;
        this.dateChange = builder.dateChange;
    }

    public static class PcPartChangeHistoryItemBuilder implements CommonModelBuilder<PcPartChangeHistoryItem> {
        private final Long pcPartChangeHistoryId;
        private final String pcFullName;
        private final String pcPartType;
        private final String pcPartName;
        private final String pcPartPrice;
        private final LocalDateTime dateChange;

        public PcPartChangeHistoryItemBuilder(PcPartChangeHistory pcPartChangeHistory) {
            this.pcPartChangeHistoryId = pcPartChangeHistory.getId();
            this.pcFullName = "[" +pcPartChangeHistory.getPcManage().getPcPlace().getName() + "]" + pcPartChangeHistory.getPcManage().getPcNumber();
            this.pcPartType = pcPartChangeHistory.getPcPartType().getName();
            this.pcPartName = pcPartChangeHistory.getPcPartName();
            this.pcPartPrice = pcPartChangeHistory.getPcPartPrice() + "원";
            this.dateChange = pcPartChangeHistory.getDateChange();
        }

        @Override
        public PcPartChangeHistoryItem build() {
            return new PcPartChangeHistoryItem(this);
        }
    }
}
