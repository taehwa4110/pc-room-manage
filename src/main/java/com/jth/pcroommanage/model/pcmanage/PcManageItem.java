package com.jth.pcroommanage.model.pcmanage;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcManageItem {
    @ApiModelProperty(notes = "PC 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "PC 위치와 번호")
    private String fullName;
    @ApiModelProperty(notes = "PC 분당 금액")
    private String pcPerMinutePrice;
    @ApiModelProperty(notes = "PC 사용 가능 여부")
    private String isUsed;
    @ApiModelProperty(notes = "PC 사용 시작일")
    private LocalDateTime dateStart;
    @ApiModelProperty(notes = "PC 고장일")
    private LocalDateTime dateBroken;

    private PcManageItem(PcItemBuilder builder) {
        this.id = builder.id;
        this.fullName = builder.fullName;
        this.pcPerMinutePrice = builder.pcPerMinutePrice;
        this.isUsed = builder.isUsed;
        this.dateStart = builder.dateStart;
        this.dateBroken = builder.dateBroken;
    }

    public static class PcItemBuilder implements CommonModelBuilder<PcManageItem> {
        private final Long id;
        private final String fullName;
        private final String pcPerMinutePrice;
        private final String isUsed;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateBroken;

        public PcItemBuilder(PcManage pcManage) {
            this.id = pcManage.getId();
            this.fullName = "[" + pcManage.getPcPlace().getName() + "]" + pcManage.getPcNumber() + "번";
            this.pcPerMinutePrice = pcManage.getPcPerMinutePrice() + "원";
            this.isUsed = pcManage.getIsUsed() ? "사용가능" : "고장";
            this.dateStart = pcManage.getDateStart();
            this.dateBroken = pcManage.getDateBroken();
        }

        @Override
        public PcManageItem build() {
            return new PcManageItem(this);
        }
    }

}
