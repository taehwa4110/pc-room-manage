package com.jth.pcroommanage.model.pcmanage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PcManagePartChangeRequest {
    @ApiModelProperty(notes = "마우스명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String mouseName;

    @ApiModelProperty(notes = "키보드명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String keyboardName;

    @ApiModelProperty(notes = "모니터명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String monitorName;

    @ApiModelProperty(notes = "헤드셋명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String headsetName;

    @ApiModelProperty(notes = "스피커명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String speakerName;

    @ApiModelProperty(notes = "CPU 장비명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String cpuName;

    @ApiModelProperty(notes = "RAM 장비명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String ramName;

    @ApiModelProperty(notes = "그래픽카드명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String graphicsCardName;
}
