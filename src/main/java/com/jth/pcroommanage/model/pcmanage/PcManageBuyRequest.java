package com.jth.pcroommanage.model.pcmanage;

import com.jth.pcroommanage.enums.PcPlace;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PcManageBuyRequest {
    @ApiModelProperty(notes = "PC 번호", required = true)
    @NotNull
    private Integer pcNumber;

    @ApiModelProperty(notes = "마우스명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String mouseName;

    @ApiModelProperty(notes = "키보드명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String keyboardName;

    @ApiModelProperty(notes = "모니터명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String monitorName;

    @ApiModelProperty(notes = "헤드셋명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String headsetName;

    @ApiModelProperty(notes = "스피커명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String speakerName;

    @ApiModelProperty(notes = "CPU 장비명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String cpuName;

    @ApiModelProperty(notes = "RAM 장비명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String ramName;

    @ApiModelProperty(notes = "그래픽카드명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String graphicsCardName;

    @ApiModelProperty(notes = "PC 위치 ", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PcPlace pcPlace;

    @ApiModelProperty(notes = "PC 분당 금액", required = true)
    @NotNull
    private Integer pcPerMinutePrice;
}
