package com.jth.pcroommanage.model.pcmanage;

import com.jth.pcroommanage.enums.PcPlace;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PcManagePlaceChangeRequest {
    @ApiModelProperty(notes = "PC 번호", required = true)
    @NotNull
    private Integer pcNumber;

    @ApiModelProperty(notes = "PC 위치 ", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PcPlace pcPlace;

    @ApiModelProperty(notes = "PC 분당 금액", required = true)
    @NotNull
    private Integer pcPerMinutePrice;
}
