package com.jth.pcroommanage.model.pcmanage;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcManageDetail {
    @ApiModelProperty(notes = "PC 번호")
    private String pcNumber;
    @ApiModelProperty(notes = "마우스명")
    private String mouseName;
    @ApiModelProperty(notes = "키보드명")
    private String keyboardName;
    @ApiModelProperty(notes = "모니터명")
    private String monitorName;
    @ApiModelProperty(notes = "헤드셋명")
    private String headsetName;
    @ApiModelProperty(notes = "스피커명 ")
    private String speakerName;
    @ApiModelProperty(notes = "CPU 장비명 ")
    private String cpuName;
    @ApiModelProperty(notes = "RAM 장비명")
    private String ramName;
    @ApiModelProperty(notes = "그래픽카드명")
    private String graphicsCardName;
    @ApiModelProperty(notes = "PC 위치 ")
    private String pcPlace;
    @ApiModelProperty(notes = "PC 분당 금액")
    private String pcPerMinutePrice;
    @ApiModelProperty(notes = "PC 사용 가능 여부")
    private String isUsed;
    @ApiModelProperty(notes = "PC 사용 시작일")
    private LocalDateTime dateStart;
    @ApiModelProperty(notes = "PC 고장일")
    private LocalDateTime dateBroken;

    private PcManageDetail(PcDetailBuilder builder) {
        this.pcNumber = builder.pcNumber;
        this.mouseName = builder.mouseName;
        this.keyboardName = builder.keyboardName;
        this.monitorName = builder.monitorName;
        this.headsetName = builder.headsetName;
        this.speakerName = builder.speakerName;
        this.cpuName = builder.cpuName;
        this.ramName = builder.ramName;
        this.graphicsCardName = builder.graphicsCardName;
        this.pcPlace = builder.pcPlace;
        this.pcPerMinutePrice = builder.pcPerMinutePrice;
        this.isUsed = builder.isUsed;
        this.dateStart = builder.dateStart;
        this.dateBroken = builder.dateBroken;
    }

    public static class PcDetailBuilder implements CommonModelBuilder<PcManageDetail> {
        private final String pcNumber;
        private final String mouseName;
        private final String keyboardName;
        private final String monitorName;
        private final String headsetName;
        private final String speakerName;
        private final String cpuName;
        private final String ramName;
        private final String graphicsCardName;
        private final String pcPlace;
        private final String pcPerMinutePrice;
        private final String isUsed;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateBroken;

        public PcDetailBuilder(PcManage pcManage) {
            this.pcNumber = pcManage.getPcNumber() + "번";
            this.mouseName = pcManage.getMouseName();
            this.keyboardName = pcManage.getKeyboardName();
            this.monitorName = pcManage.getMonitorName();
            this.headsetName = pcManage.getHeadsetName();
            this.speakerName = pcManage.getSpeakerName();
            this.cpuName = pcManage.getCpuName();
            this.ramName = pcManage.getRamName();
            this.graphicsCardName = pcManage.getGraphicsCardName();
            this.pcPlace = pcManage.getPcPlace().getName();
            this.pcPerMinutePrice = pcManage.getPcPerMinutePrice() + "원";
            this.isUsed = pcManage.getIsUsed() ? "사용가능" : "고장";
            this.dateStart = pcManage.getDateStart();
            this.dateBroken = pcManage.getDateBroken();
        }

        @Override
        public PcManageDetail build() {
            return new PcManageDetail(this);
        }
    }
}
