package com.jth.pcroommanage.controller;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.pcusagehistory.PcUsageHistoryItem;
import com.jth.pcroommanage.model.pcusagehistory.PcUsageHistoryRequest;
import com.jth.pcroommanage.service.PcManageService;
import com.jth.pcroommanage.service.PcUsageHistoryService;
import com.jth.pcroommanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "PC 사용 내역 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pc-usage-history")
public class PcUsageHistoryController {
    private final PcUsageHistoryService pcUsageHistoryService;
    private final PcManageService pcManageService;


    @ApiOperation(value = "PC 사용 내역 등록하기")
    @PostMapping("/new/{pcManageId}")
    public CommonResult setPcUsageHistory(@PathVariable long pcManageId, @RequestBody @Valid PcUsageHistoryRequest request) {
        PcManage pcManage = pcManageService.getPcData(pcManageId);
        pcUsageHistoryService.setPcUsageHistory(pcManage, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간에 해당되는 PC 사용내역 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<PcUsageHistoryItem> getPcUsageHistories(
            @RequestParam(value = "dateStart")@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateStart,
            @RequestParam(value = "dateEnd")@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd) {
        return ResponseService.getListResult(pcUsageHistoryService.getPcUsageHistory(dateStart,dateEnd), true);
    }
}
