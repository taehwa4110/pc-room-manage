package com.jth.pcroommanage.controller;

import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.pcpartchangehistory.PcPartChangeHistoryItem;
import com.jth.pcroommanage.model.pcpartchangehistory.PcPartChangeHistoryRequest;
import com.jth.pcroommanage.service.PcManageService;
import com.jth.pcroommanage.service.PcPartChangeHistoryService;
import com.jth.pcroommanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "PC 부품 교체 내역 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pc-part-change-history")
public class PcPartChangeHistoryController {
    private final PcPartChangeHistoryService pcPartChangeHistoryService;
    private final PcManageService pcManageService;

    @ApiOperation(value = "PC 부품 교체 내역 등록하기")
    @PostMapping("/new")
    public CommonResult setPcPartChangeHistory(@RequestBody @Valid PcPartChangeHistoryRequest request) {
        PcManage pcManage = pcManageService.getPcData(request.getPcManageId());
        pcPartChangeHistoryService.setPcPartChangeHistory(pcManage, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간에 해당되는 PC 부품 교체내역 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<PcPartChangeHistoryItem> getPcPartChangeHistory(
            @RequestParam(value = "dateStart")@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateStart,
            @RequestParam(value = "dateEnd")@DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd) {
        return ResponseService.getListResult(pcPartChangeHistoryService.getPcPartChangeHistories(dateStart,dateEnd), true);
    }
}
