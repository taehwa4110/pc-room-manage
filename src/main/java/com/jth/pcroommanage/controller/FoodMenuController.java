package com.jth.pcroommanage.controller;

import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.foodmenu.FoodMenuItem;
import com.jth.pcroommanage.model.foodmenu.FoodMenuRequest;
import com.jth.pcroommanage.model.foodmenu.FoodPriceUpdateRequest;
import com.jth.pcroommanage.service.FoodMenuService;
import com.jth.pcroommanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "음식메뉴 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/food-menu")
public class FoodMenuController {
    private final FoodMenuService foodMenuService;

    @ApiOperation(value = "음식메뉴 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setFoodMenu(@RequestBody @Valid FoodMenuRequest foodMenuRequest) {
        foodMenuService.setFoodMenu(foodMenuRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "음식메뉴 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<FoodMenuItem> getFoods() {
        return ResponseService.getListResult(foodMenuService.getFoods(), true);
    }

    @ApiOperation(value = "음식 가격 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "음식메뉴 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putFoodPrice(@PathVariable long id, @RequestBody @Valid FoodPriceUpdateRequest request) {
        foodMenuService.putFoodPrice(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "음식 메뉴 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "음식메뉴 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delFood(@PathVariable long id) {
        foodMenuService.putFoodSaleClose(id);

        return ResponseService.getSuccessResult();
    }
}
