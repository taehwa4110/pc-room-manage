package com.jth.pcroommanage.controller;

import com.jth.pcroommanage.entity.FoodMenu;
import com.jth.pcroommanage.entity.PcManage;
import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderCurrentSituationChangeRequest;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderHistoryItem;
import com.jth.pcroommanage.model.foodorderhistory.FoodOrderHistoryRequest;
import com.jth.pcroommanage.service.FoodMenuService;
import com.jth.pcroommanage.service.FoodOrderHistoryService;
import com.jth.pcroommanage.service.PcManageService;
import com.jth.pcroommanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "음식 주문 내역 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/food-order-history")
public class FoodOrderHistoryController {
    private final FoodOrderHistoryService foodOrderHistoryService;
    private final PcManageService pcManageService;
    private final FoodMenuService foodMenuService;

    @ApiOperation(value = "음식 주문 내역 등록하기")
    @PostMapping("/new")
    public CommonResult setFoodOrderHistory(@RequestBody @Valid FoodOrderHistoryRequest request) {
        PcManage pcManage = pcManageService.getPcData(request.getPcManageId());
        FoodMenu foodMenu = foodMenuService.getFoodMenuData(request.getFoodMenuId());
        foodOrderHistoryService.setFoodOrderHistory(pcManage, foodMenu, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간에 해당되는 음식 주문 내역 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<FoodOrderHistoryItem> getFoodOrderHistories(
            @RequestParam(value = "dateStart")@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd")@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd) {
        return ResponseService.getListResult(foodOrderHistoryService.getFoodOrderHistory(dateStart,dateEnd), true);
    }

    @ApiOperation(value = "음식 현재 상황 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "음식 주문 내역 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putFoodOrderCurrentSituation(@PathVariable long id, @RequestBody @Valid FoodOrderCurrentSituationChangeRequest request) {
        foodOrderHistoryService.putFoodOrderCurrentSituation(id, request);

        return ResponseService.getSuccessResult();
    }
}
