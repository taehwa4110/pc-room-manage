package com.jth.pcroommanage.controller;

import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.model.ListResult;
import com.jth.pcroommanage.model.SingleResult;
import com.jth.pcroommanage.model.pcmanage.*;
import com.jth.pcroommanage.service.PcManageService;
import com.jth.pcroommanage.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "PC 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pc-manage")
public class PcManageController {
    private final PcManageService pcManageService;

    @ApiOperation(value = "PC 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setPc(@RequestBody @Valid PcManageBuyRequest request) {
        pcManageService.setPc(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "PC 상세정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PC 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<PcManageDetail> getPc(@PathVariable long id) {
        return ResponseService.getSingleResult(pcManageService.getPc(id));
    }

    @ApiOperation(value = "PC 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<PcManageItem> getPcs() {
        return ResponseService.getListResult(pcManageService.getPcs(), true);
    }

    @ApiOperation(value = "PC 부품 교체하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PC 시퀀스", required = true)
    })
    @PutMapping("/part-change/{id}")
    public CommonResult putPcPart(@PathVariable long id, @RequestBody @Valid PcManagePartChangeRequest request) {
        pcManageService.putPcPart(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "PC 설치위치 교체하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PC 시퀀스", required = true)
    })
    @PutMapping("/pc-place/{id}")
    public CommonResult putPcPlace(@PathVariable long id, @RequestBody @Valid PcManagePlaceChangeRequest request) {
        pcManageService.putPcPlace(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "PC 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PC 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delPc(@PathVariable long id) {
        pcManageService.putPcBroken(id);

        return ResponseService.getSuccessResult();
    }


}
