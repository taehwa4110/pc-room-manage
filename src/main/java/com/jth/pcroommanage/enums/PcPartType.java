package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcPartType {
    MOUSE("마우스"),
    KEYBOARD("키보드"),
    MONITOR("모니터"),
    HEADSET("헤드셋"),
    SPEAKER("스피커"),
    CPU("CPU"),
    RAM("RAM"),
    GRAPHICSCARD("그래픽카드");

    private final String name;
}
