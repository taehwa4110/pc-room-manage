package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FoodType {
    NOODLES("면류"),
    RICE("밥류"),
    HOTDOG("핫도그"),
    BEVERAGE("음료"),
    FRIED("튀김"),
    SNACKS("과자");

    private final String name;
}
