package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PaymentType {
    PREPAYMENT("선불"),
    POSTPAYMENT("후불");

    private final String name;
}
