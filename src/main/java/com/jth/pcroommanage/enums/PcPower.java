package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcPower {
    ON("컴퓨터 하는중"),
    OFF("컴퓨터 종료");

    private final String name;
}
