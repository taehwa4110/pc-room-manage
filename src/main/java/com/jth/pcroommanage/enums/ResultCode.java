package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    ,FAILED(-1, "실패하였습니다.")

    ,MISSING_DATA(-10000,"데이터를 찾을 수 없습니다.")

    , NO_PC_DATA(-20000,"PC가 고장났습니다.")
    , NO_FOOD_DATA(-30000,"음식 메뉴가 없습니다.");

    private final Integer code;
    private final String msg;
}
