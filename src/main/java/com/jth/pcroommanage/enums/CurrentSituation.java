package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CurrentSituation {
    ORDERSUCCESS("주문 완료"),
    FOODMAKING("음식 조리중"),
    DELIVERYCOMPLETE("배달 완료");

    private final String name;
}
