package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcPlace {
    NORMAL("일반석"),
    COUPLE("커플석"),
    PREMIUM("프리미엄석");

    private final String name;
}
