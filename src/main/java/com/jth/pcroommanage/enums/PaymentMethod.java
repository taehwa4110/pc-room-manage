package com.jth.pcroommanage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PaymentMethod {
    CASH("현금"),
    CARD("카드");

    private final String name;
}
