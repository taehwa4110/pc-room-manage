package com.jth.pcroommanage.advice;

import com.jth.pcroommanage.enums.ResultCode;
import com.jth.pcroommanage.exception.CMissingDataException;
import com.jth.pcroommanage.exception.CNoFoodDataException;
import com.jth.pcroommanage.exception.CNoPcDataException;
import com.jth.pcroommanage.model.CommonResult;
import com.jth.pcroommanage.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoPcDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoPcDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_PC_DATA);
    }

    @ExceptionHandler(CNoFoodDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoFoodDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_PC_DATA);
    }
}
