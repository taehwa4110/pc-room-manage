### PC방 관리 API
***
(개인프로젝트) PC방을 관리하는 API
***

### Language
```
JAVA 16
SpringBoot 2.7.3
```

### 기능
``` 
* PC 관리
 - PC 등록
 - PC 상세 정보 가져오기
 - PC 리스트 가져오기
 - PC 부품 교체하기
 - PC 설치 위치 교체하기
 - PC 정보 삭제하기
 
* PC 사용내역 관리
 - PC 사용내역 등록
 - 기간내 해당되는 PC 사용내역 리스트 가져오기
 
* PC 부품 교체내역 관리
 - PC 부품 교체내역 등록
 - 기간내 해당되는 PC 부품 교체내역 리스트 가져오기 

* 음식 메뉴 관리
 - 음식 메뉴 등록
 - 음식 메뉴 리스트 가져오기
 - 음식 가격 수정하기
 - 음식 메뉴 삭제하기
 
* 음식 주문내역 관리
 - 음식 주문내역 등록
 - 기간내 해당되는 음식 주문내역 리스트 가져오기
 

```

### 예시
>* 스웨거 전체 화면
>
>![swagger_all](./images/swagger-all.png)

> * 스웨거 PC 관리 화면
>
>![swagger_pc](./images/swagger-pc.png)

> * 스웨거 PC 사용내역 관리 화면
>
>![swagger_pc_usage_history](./images/swagger-pc-usage-history.png)

> * 스웨거 PC 부품 교채내역 관리 화면
>
>![swagger_pc_part_change_history](./images/swagger-pc-part-change-history.png)

>* 스웨거 음식 메뉴 관리 화면
>
>![swagger_pc_food_menu](./images/swagger-pc-food-menu.png)

>* 스웨거 음식 주문내역 관리 화면
>
>![swagger_food_order_history](./images/swagger-food-order-history.png)